# Go to the directory that has your Dockerfile and run the following command to build the Docker image. 
# The -t flag lets you tag your image so it's easier to find later using the docker images

### `$ docker build . -t <your username>/node-web-app`

# Your image will now be listed by Docker:

### `$ docker images`

# Running your image with -d runs the container in detached mode, leaving the container running in the background. 
# The -p flag redirects a public port to a private port inside the container. Run the image you previously built:

### `docker run -p 49160:8080 -d <your username>/node-web-app / --name node-app` 
